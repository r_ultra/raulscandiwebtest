'use strict';
$(document).ready(function() {

if (window.location.href.indexOf('add-product') > -1 || window.location.href.indexOf('products/add') > -1) {
	
    $.validate({
        lang: 'en'
    });

	// refresh selection area
    $('#productType').change(refresh_inputs);
        refresh_inputs();

        function refresh_inputs() {
            var val = $('#productType').val(); //type selected

            var typeForm = $('.form-type-product');
            typeForm.empty();
            
            switch (val) {
              case 'Dvd':
                typeForm.append(
              `<div id="Dvd" class="form-field col-lg-10 ">
                <p class="text-muted lead">Please provide disc space in MB</p>
                <input id="size" min="1"  class="input-text js-input form-control input-type-product" type="number" name="size" placeholder="Size in MB" style="margin: 1em">
               </div>  `);
                $('#size').attr('data-validation','required length number').attr('data-validation-length','1-11');
                break;
              case 'Book':
                typeForm.append(
              `<div id="Book" class="form-field col-lg-10" >
                <p class="text-muted lead">Please provide weight in Kg</p>
                <input id="weight" min="1"  class="form-group js-input form-control input-type-product" type="number" name="weight" placeholder="weight" style="margin: 1em;" >
               </div>  `);
                $('#weight').attr('data-validation','required length number').attr('data-validation-length','1-11');
                break;
              case 'Furniture':
                typeForm.append(
              `<div id="Furniture" class="form-field col-lg-10  ">
                <p class="text-muted lead">Please provide dimensions in HxWxL format</p>
                <input id="height"  class="input-text js-input form-control input-type-product" type="number"  min="1"name="height" placeholder="Height" style="margin: 1em;" >
                <input id="width"  class="input-text js-input form-control input-type-product" type="number" min="1" name="width" placeholder="Width" style="margin: 1em;">
                <input id="length" class="input-text js-input form-control input-type-product" type="number" min="1" name="length" placeholder="Length" style="margin: 1em;">
              </div>  `);
                $('#height').attr('data-validation','required length number').attr('data-validation-length','1-11');
                $('#width').attr('data-validation','required length number').attr('data-validation-length','1-11');
                $('#length').attr('data-validation','required length number').attr('data-validation-length','1-11');
                break;
            }

        }
            

}
});