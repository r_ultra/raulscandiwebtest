<?php
//Require libraries from folder libraries
require_once 'libraries/Debug.php';
require_once 'libraries/Core.php';
require_once 'libraries/Controller.php';
require_once 'libraries/db.php';
require_once 'config/config.php';

//Instantiate core class
$init = new Core();
