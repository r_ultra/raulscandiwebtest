<?php 
//require_once 'controllers/interfaces/productInterface.php';

class Furniture extends product //implements productInterface
{

    private $height;
    private $width;
    private $lenght;


    function __construct()
    {
        $this->db = new MySqlDb();
    }

	public function getInfo($prodForm)
	 {
	 	echo $prodForm['width'].$prodForm['height'].$prodForm['length'] ." Furniture";
	 }

	public function mensaje()
	{
		$mensaje = "<h1>Mensaje furniture</h1>";
		return $mensaje;
	}


    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $lenght
     *
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }



     /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type,$data)
    {
        $this->type = $type;
        $this->setHeight($data['height']);
        $this->setWidth($data['width']);
        $this->setLength($data['length']);
        return $this;
    }


    public function save()
    {
        if ($this->db->createData('product',
                "(SKU,Name,Price,Type)",
                    "(
                    '{$this->getSku()}',
                    '{$this->getName()}',
                    {$this->getPrice()},
                    '{$this->getType()}'
                    )"
                )
            )
        {
           $this->db->createData('typefurniture',
                        '(SKU,Height,Width,Length)',
                            "(
                            '{$this->getSku()}',
                            {$this->getHeight()},
                            {$this->getWidth()},
                            {$this->getLength()}
                            )"
                        );
           return true;
        }else{
            return false;
        }

    }

}


 ?>