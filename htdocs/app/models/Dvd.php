<?php 
//require_once 'controllers/interfaces/productInterface.php';

class Dvd extends product //implements productInterface
{
    private $size;

    function __construct()
    {
        $this->db = new MySqlDb();
    }

	public function getInfo($prodForm)
	 {
	 	echo $prodForm['size']." Dvd";
	 }

	public function mensaje()
	{
		echo "string";
	}

     /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     *
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

     /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type,$data)
    {
        $this->type = $type;
        $this->setSize($data['size']);
        return $this;
    }
    
    public function save()
    {

        if ($this->db->createData('product',
                "(SKU,Name,Price,Type)",
                    "(
                    '{$this->getSku()}',
                    '{$this->getName()}',
                    {$this->getPrice()},
                    '{$this->getType()}'
                    )"
                )
            )
        {
           $this->db->createData('typedvd',
                        '(SKU,Size)',
                            "(
                            '{$this->getSku()}',
                            {$this->getSize()}
                            )"
                        );
           return true;
        }else{
            return false;
        }

    }


}


 ?>