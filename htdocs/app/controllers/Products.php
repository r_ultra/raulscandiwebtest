<?php 
// /home/vol6_8/epizy.com/epiz_30291775/htdocs/app
class Products extends Controller
{
	private $book;
	private $products;
	
	function __construct()
	{
		$this->productModel = $this->model('Product');

	}

	public function index()
	{
		//die;
		//renderizar vista
		//$products = $this->productModel->search();
		$data = ['location' => 'index'];

        //Buscar posts por tipo de post
        if ($products = $this->productModel->search()) {
            $data = [
            'location' => 'index',
            'products' => $products,
            ];

        }
        else {
                $data['errMsg'] = 'Nothing here 😥';
        }

        $this->view('product/index', $data);
        return $data;

	}

	public function delete()
	{
		if ($_POST) {
			$selections = array_values($_POST);

			if ($this->productModel->delete($selections)) {
				header('Location: '.URLROOT);
			}else{
				return false;
			}
		}else{
			header('Location: '.URLROOT);
		}

	}

	public function add()
	{
		$data = array();
		if (isset($_POST)) {
			
			$sku= isset($_POST['sku']) 
			? $_POST['sku'] : false ;

			$name= isset($_POST['name']) 
			? $_POST['name'] : false ;

			$price= isset($_POST['price']) 
			? floatval($_POST['price']) : false ;

			$type= isset($_POST['type']) 
			? $_POST['type'] : false ;
			
			if ($sku and $name and $price and $type) {

				//save the form data to be used in the correct class
				$productType = $type; // can be book, furniture, dvd ...
				$prodForm = $_POST;

				// create the correct instance avoiding the use of conditional statements
				$productCorrect = $this->model($type);
				
				/* needed if i use interfaces
				$refl = new ReflectionClass($productType);
				$instance = $refl->newInstanceArgs();
				

				
				// return the correct product class
				$productCorrect = $this->product->selectProduct($instance);
				*/

				$productCorrect->setSku($sku);
				$productCorrect->setName($name);
				$productCorrect->setPrice($price);
				$productCorrect->setType($type,$prodForm);
				if($productCorrect->save()){
					header('Location: '.URLROOT);
					return true;
				}else{
					$data['errMsg'] = 'Error with your data, try with another SKU';
					header('Location: '.URLROOT);
					return false;
				}
			}
		}
		$this->view('product/add', $data);
	}


}


?>




