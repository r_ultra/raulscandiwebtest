<?php
class Pages extends Controller
{
    public function __construct()
    {
        //$this->userModel = $this->model('User');
        $this->log("app/controllers/Pages::_construct() => New page object...");
    }

    public function index()
    {
        $data = [
            'title' => 'Home page',
        ];
        $this->log("app/controllers/Pages::index() => Rendering...");
        $this->view('index', $data);
    }

}