<?php 

?>
<nav >
  <div >
  <h2 class="text-muted">Product List</h2>
    <div class="buttons">
        <input class="buttons btn btn-success btn-sm" type="submit" form="product_form" value="Save" />
        <a href="<?= URLROOT ?>" class="buttons btn btn-warning btn-sm  ">Cancel</a>
    </div>
  </div>
  <hr>  
</nav>
<div id="content">

<div class="container">

<form class="well form-horizontal" action="<?=URLROOT?>/products/add" method="POST"  id="product_form">


<fieldset>

<!-- Form Name -->
<legend><center><h2><b>Product Add</b></h2></center></legend><br>
<?php 
  if(isset($data['errMsg'])):
    echo "<div class='alert alert-danger'>".$data['errMsg']." 😢</div>";
  endif;
?>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sku">SKU</label>  
  <div class=" inputGroupContainer">
	  <div class="input-group">
	  	<input  name="sku" placeholder="Product ID" id="sku" class="form-control"  type="text"
      data-validation="required length"
      data-validation-length="1-20"
      >
	  </div>
  </div>
</div>

<?php
  // $apiPrefix = "https://random-words-api.vercel.app/word";
  //   $data = file_get_contents("{$apiPrefix}", false);
  //   $result = json_decode($data);
  //   $default = $result[0];
?>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Name</label>  
  <div class=" inputGroupContainer">
  <div class="input-group">

  <input  name="name" placeholder="The name of your product" id="name" class="form-control"  type="text"
      data-validation="required length"
      data-validation-length="1-200">
    </div>
  </div>
</div>


<!-- Number input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="price">Price</label>  
  <div class=" inputGroupContainer">
  <div class="input-group">
  	<input  name="price" placeholder="Price in $" id="price" class="form-control"  type="number" min="1" 
     data-validation="required length number"
      data-validation-length="1-5"
     >
   </div>
  </div>
</div>



  <div class="form-group">
  <label class="col-md-4 control-label" for="productType">Type</label>
    <div class=" selectContainer">
    <div class="input-group">

    <select id="productType" class="form-control selectpicker" name="type"
        >
		    <option value="Book" selected>Book</option>
		    <option value="Furniture" >Furniture</option>
		    <option value="Dvd" >DVD</option>
    </select>

		  <div id="type" class="input-group inputGroupContainer form-group form-type-product">

      </div>
		</div>
  	</div>
   </div>

</fieldset>
</form>
</div>

<!-- Select the type of product -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
