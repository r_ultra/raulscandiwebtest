<?php 

 ?>


<!-- define('URLROOT', 'http://raul.great-site.net'); -->
<nav >
	<div >
	<h2 class="text-muted">Product List</h2>
		<div class="buttons">
			<a href="<?= URLROOT ?>/add-product" class="buttons btn btn-success btn-sm  " >ADD</a>
			<input class="buttons btn btn-warning btn-sm" type="submit" form="selectForm" value="MASS DELETE" id="delete-product-btn" />
		</div>
	</div>
	<hr>	
</nav>

<div id="content">
<?php 
	if(isset($data['errMsg'])):
		echo "<div class='alert alert-danger'>".$data['errMsg']."</div>";
	endif;
?>
<form id="selectForm" action="<?=URLROOT?>/products/delete" method="POST">
<section class="gallery-block cards-gallery">
	<div class="container">

			<div class="row">
<?php 
				if (!empty($data['products'])):
				foreach ($data['products'] as $product):
				?>
				<div class="col-md-4 col-lg-4">

					<div class="card" style="width: 18rem; margin: 1em;">
					  <div class="card-body">
					    <h5 class="card-title"><?= $product['Name']?></h5>
					    <h6 class="card-subtitle mb-2 text-muted">SKU: <?= $product['SKU'] ?></h6>
					    <p class="card-text">Price: <?= $product['Price'] ?>$</p>
					    <?php 
					    // var_dump($data);
					    // die;
					    if (isset($product['Weight'])): 
					    	$data = 'Weight: '.$product['Weight'].' Kg';

						elseif (isset($product['Size'])):
					    	$data = 'Size: '.$product['Size'].' MB';
						elseif (isset($product['Height'])):
							$data = 'Dimensions: '.
								$product['Height'].'x'.$product['Width'].'x'.$product['Length'] ;
					    endif;
					  
					    ?>

					    <p class="card-text text-muted"><?= $data ?></p>

					    <div class="form-check">
					    	<input type="checkbox" class="delete-checkbox" 
					    		name="<?=$product['SKU']?>" 
					    		value="<?= $product['SKU'] ?>">
					  	</div>
					  </div>
					</div>

				</div>

			<?php 
			$data=0;
			endforeach; 
			endif;
			?>
			</div>
 
		</form>

			
	</div>
</section>

</form>

