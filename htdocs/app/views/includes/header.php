<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?=URLROOT?>/public/assets/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?=URLROOT?>/public/assets/js/main.js"></script>

	<title>Product List</title>
</head>
<body>
<div id="container">

<!-- CAbecera -->
<header id="header">
	<div id="logo">
		<img src="<?=URLROOT?>/public/assets/images/scandiweb_logo.png" alt="Scandiweb logo"/>
		<a href="<?=URLROOT?>">Test Task</a>
	</div>
</header>
