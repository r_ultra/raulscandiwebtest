<?php
//Load the model and the view
class Controller extends Debug
{
    public function model($model)
    {
        //Require model file
        require_once '../app/models/' . $model . '.php';
        //Instantiate model
        $this->log("app/libraries/Controller::model() => Cargando nuevo modelo $model...");
        return new $model();
    }

    //Load the view (checks for the file)
    public function view($view, $data = [])
    {
        if (file_exists('../app/views/' . $view . '.php')) {
            require_once '../app/views/includes/header.php';
            require_once '../app/views/' . $view . '.php';
            require_once '../app/views/includes/footer.php';
        } else {
            require_once '../app/views/includes/header.php';
            require_once '../app/views/errors/error404.php';
            require_once '../app/views/includes/footer.php';
        }
    }

}