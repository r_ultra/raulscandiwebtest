<?php
/*
 * App Core Class
 * Creates URL & loads core controller
 * URL FORMAT - /controller/method/params
 */
class Core extends Debug
{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
        $this->log("app/libraries/Core::_construct() => New Core interaction...");
        //print_r($this->getUrl());

        $url = $this->getUrl();
        $pageMethod = true;

        //Make sure $url[0] exists or is not empty
        if (isset($url[0])) {
            if ($url[0] == 'add-product' ) {
                $url[0] = "products";
                $url[1] = "add";                
            }
        }else{
                $url[0] = "products";
                $url[1] = "index";                
        }
           
            // Look in BLL for first value
            if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
                $this->log("app/libraries/Core::_construct() => url[0] (" . $url[0] . ") exists and is a controller...");
                // If exists, set as controller
                $this->currentController = ucwords($url[0]);
                // Unset 0 Index
                unset($url[0]);
                // Is not a page method
                $pageMethod = false;
            }

        // Require the controller
        require_once '../app/controllers/' . $this->currentController . '.php';

        // Instantiate controller class
        $this->currentController = new $this->currentController;        
        // Check for second part of url
        if (isset($url[1])) {

            // Check to see if method exists in controller
            if (method_exists($this->currentController, $url[1])) {
                $this->log("app/libraries/Core::_construct() => url[1] (" . $url[1] . ") exists and is a method...");
                $this->currentMethod = $url[1];
                // Unset 1 index
                unset($url[1]);
                
            }
        } else {

            if ($pageMethod == true and isset($url[0])) {
                // Check to see if method exists in controller
                if (method_exists($this->currentController, $url[0])) {
                    $this->log("app/libraries/Core::_construct() => url[0] (" . $url[0] . ") exists and is a method of Page controller...");
                    $this->currentMethod = $url[0];
                    // Unset 0 index
                    unset($url[0]);
                }
            } else {
                $this->currentMethod = 'index';
                
            }
        }

        // Get params
        $this->params = $url ? array_values($url) : [];

        if ($url) {
            $aux = '[';
            $cont = 0;
            foreach ($this->params as $p) {
                $aux .= $cont > 0 ? ",$p" : "$p";
                $cont++;
            }
            $aux .= ']';
            $this->log("app/libraries/Core::_construct() => params $aux");
        }

        // Call a callback with array of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    public function getUrl()
    {
        $url = [];
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
        }

        return $url;
    }
}