<?php
class Database extends Controller
{
    private $dbHost = DB_HOST;
    private $dbUser = DB_USER;
    private $dbPass = DB_PASS;
    private $dbName = DB_NAME;

    private $statement;
    private $dbHandler;
    private $error;

    public function __construct()
    {
        $conn = 'mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );

        try {
            $this->dbHandler = new PDO($conn, $this->dbUser, $this->dbPass, $options);
        } catch (PDOException $e) {
            
            echo'<script type="text/javascript">
            alert("No se pudo conectar a Base de Datos");
            </script>';
            header( "refresh:1; url=". URLROOT . "/pages/index" );
            exit;
            $this->error = $e->getMessage();
            
        }

    }

    //Allows us to write queries
    public function query($sql)
    {
        $this->statement = $this->dbHandler->prepare($sql);
    }

    //Bind values
    public function bind($parameter, $value, $type = null)
    {
        switch (is_null($type)) {
            case is_int($value):
                $type = PDO::PARAM_INT;
                break;
            case is_bool($value):
                $type = PDO::PARAM_BOOL;
                break;
            case is_null($value):
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;
        }
        $this->statement->bindValue($parameter, $value, $type);

        
    }

    //Execute the prepared statement
    public function execute()
    {
        try {
            return $this->statement->execute();    
        } catch (Exception $e) {
            return false;
            exit;   
        }
        
        
    }

    //Return an array
    public function resultSet()
    {   
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_OBJ);
    }

    //Return a specific row as an object
    public function single()
    {
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_OBJ);
        
    }

    //Get's the row count
    public function rowCount()
    {
        
        try {
            return $this->statement->rowCount();
        } catch (Exception $e) {
            echo'<script type="text/javascript">
            alert("Error en Base de datos");
            </script>';
            header( "refresh:1; url=".getenv('HTTP_REFERER') );
            exit;            
        }
        
    }
}