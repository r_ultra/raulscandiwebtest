<?php
class Debug
{
    private $debug_mode = false;

    /**
     * Create logs to debug.log file
     * @param string $msg message to prompt in debug.log
     */
    public function log($msg)
    {
        //Create logs only on debug mode on true
        if ($this->debug_mode) {
            error_reporting(E_ALL);
            ini_set('ignore_repeated_errors', true);
            ini_set('display_errors', true);
            ini_set('log_errors', true);
            ini_set("error_log", "debug.log");
            error_log($msg);
        }
    }
}