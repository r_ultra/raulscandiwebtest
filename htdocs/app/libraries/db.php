<?php 

class MySqlDb{
    protected $host;
    protected $username;
    protected $password;
    protected $db;
    protected $conn;

    function __construct(){
        $this->host     = hostDb;
        $this->username = userDb ;
        $this->password = passDb ;
        $this->db       = nameDb ;      
        $this->connect();
  
        }
    private function connect(){
            $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db);
            if ($this->conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error." ".$this->host." ". $this->username." ". $this->password." ".$this->db);
            }
        }

    public function findAllProducts()
    {
        $prod = $this->db->query("Select * from productos;");
        $productos=$prod->fetch_all(MYSQLI_ASSOC);
        return $productos;
    }


    public function getData($table, $where){
            $this->connect();
            $sql = "SELECT * FROM ".$table. " WHERE ".$where." ORDER BY SKU";          
            $sql = $this->conn->query($sql);
            // var_dump($sql);
            // die;
            $products = $sql->fetch_all(MYSQLI_ASSOC);
            return $products;
    }

    public function createData($table, $columns, $values){
            $this->connect();
            $query = "INSERT INTO ".$table." ".$columns." VALUES ".$values;
            
            $sql = $this->conn->query($query);
            if($sql == true){
                return $sql;
            }else{
                //echo mysqli_error($this->conn);
                //echo $query;
                return false;
            }
    }

    function deleteData($table, $filter){
       
        $this->connect();
        $sql =  "DELETE FROM `".$table."` ".$filter;  
        echo $sql.'<br>';
        $sql = $this->conn->query($sql);
        if($sql == true){
            return true;
        }else{
            return false;
        }
    }
       
}

?>